import os
import time
import RPi.GPIO as GPIO
import string
from evdev import InputDevice
from select import select
from pymongo import MongoClient
import ssl
import picamera
import json
import io

R = 6
G = 13
B = 5

isLocked = 1

lockHall = 27
unlockHall = 17
doorHall = 22

lockBolt = 12
unlockBolt = 19 

GPIO.setmode(GPIO.BCM)
GPIO.setup(B, GPIO.OUT)
GPIO.setup(R, GPIO.OUT)
GPIO.setup(G, GPIO.OUT)
GPIO.setup(lockBolt, GPIO.OUT)
GPIO.setup(unlockBolt, GPIO.OUT)
GPIO.output(lockBolt,1)
GPIO.output(unlockBolt,1)
GPIO.setup(lockHall, GPIO.IN,pull_up_down=GPIO.PUD_UP)
GPIO.setup(unlockHall, GPIO.IN,pull_up_down=GPIO.PUD_UP)
GPIO.setup(doorHall, GPIO.IN,pull_up_down=GPIO.PUD_UP)

def boltCheck(pin):
    locked = GPIO.input(lockHall)
    unlocked = GPIO.input(unlockHall)
    doorClosed = GPIO.input(doorHall)
    if pin == lockHall:
	if not locked:
	    print 'Locked'
	else:
	    print 'Not Locked'
    if pin == unlockHall:
	if not unlocked:
	    print 'Unlocked'
	else:
	    print 'Not Unlocked'
    if pin == doorHall:
	if not doorClosed:
	    print 'Door Closed'
	else:
	    print 'Door Opened'
    if (locked == 0) and (doorClosed == 0):
        status(1,0,0)
    if(unlocked == 0) and (doorClosed == 0):
        status(0,1,0)
	islocked = 0
    if(unlocked == 0) and (doorClosed == 1):
        status(0,1,0)

GPIO.add_event_detect(lockHall,GPIO.BOTH,callback=boltCheck)
GPIO.add_event_detect(unlockHall,GPIO.BOTH,callback=boltCheck)
GPIO.add_event_detect(doorHall,GPIO.BOTH,callback=boltCheck)


def status(r,g,b):
        GPIO.output(R,r)
        GPIO.output(G,g)
        GPIO.output(B,b)

status(1,1,0)

    
def lock():
    print "waiting for door to close before locking"
    while GPIO.input(doorHall):
        time.sleep(.1)
#	print 'fuck'
    time.sleep(.1)
#    print GPIO.input(lockHall)
#    while GPIO.input(lockHall):
    print "Locking"
    GPIO.output(lockBolt,0)
    time.sleep(.25)
    GPIO.output(lockBolt,1)
 #   if GPIO.input(lockHall):
    time.sleep(3)
    print "Locking"
    GPIO.output(lockBolt,0)
    time.sleep(.25)
    GPIO.output(lockBolt,1)
  # if GPIO.input(lockHall):
    time.sleep(3)
    print "Locking"
    GPIO.output(lockBolt,0)
    time.sleep(.25)
    GPIO.output(lockBolt,1)
   # if GPIO.input(lockHall):
    time.sleep(3)

def unlock():
    msg = ""
    while GPIO.input(unlockHall):
	print "Unlocking"+msg
        GPIO.output(unlockBolt,0)
        time.sleep(.25)
        GPIO.output(unlockBolt,1)
        if GPIO.input(unlockHall):
	    msg = ", bolt jammed, try lightly pushing or pulling on the handle"
            time.sleep(3)
    print "Waiting for door to open before exiting unlock cycle"   
    while not GPIO.input(doorHall):
        time.sleep(.1)




def readSerialNum():
        serial = ""
        keys = "XX1234567890"
        r,w,x = select([reader],[],[])
        for event in reader.read_loop():
                if event.type==1 and event.value==1:
                        if event.code != 28:
                                serial += keys[event.code]
                        else:
                                return serial

def checkList(idList,id):
        for authed in idList:
                if authed['serial'] == id:
                        return authed
        return False

try:
        with open('config.json','r') as confFile:
                conf = json.load(confFile)
except IOError:
        print 'Unable to open config.json'
except: 
        print 'config.json error, check the format of the config file'

if conf:
        try:
                mongo = MongoClient(host=conf['mongo']['host'],port=conf['mongo']['port'],ssl=True, ssl_certreqs=conf['mongo']['ssl_cert_req'])
                db = mongo.get_database(conf['mongo']['db'])
                db.authenticate(name=conf['mongo']['name'],password=conf['mongo']['password'],mechanism=conf['mongo']['mechanism'])
	except:
                print
		print "Unable to connect to mongodb"
else:
        print "No valid Config.json, skipping mongodb connection"

try:
        reader = InputDevice('/dev/input/by-id/usb-Sycreader_RFID_Technology_Co.__Ltd_SYC_ID_IC_USB_Reader_08FF20140315-event-kbd')
except:
        if conf:
                print "Card Reader Error, shit's fucked call: " + conf['support']['phone']
        else:
                print "Card Reader Error, call the administrator!"
        exit




try:
         with open("authList/roles.json","r") as roleFile:
                 authList = json.load(roleFile)
except:
        print 'Error, ensure /authList/roles.json exists and has at least one ID in the "admin" object'
        exit


read = ""
while True:
    read = readSerialNum()
    if GPIO.input(unlockHall):
	isLocked = 1
    else:
	isLocked = 0
    if checkList(authList['spaceUsers'],read) or checkList(authList['admins'],read):
	if isLocked:
            unlock()
            unlockedAt = time.clock()
            lock()
    	elif not(GPIO.input(doorHall) or GPIO.input(unlockHall)):
	    lock()


    read = ""
