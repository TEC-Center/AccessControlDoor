## The Automatic Access control Door

### Connection Guide

![Raspberry Pi Pinout](images/RP2_Pinout.png "Raspberry Pi Pinout")

|wire color | pin | gpio | description          |function|
|---|---|---|---|---|
|Red (2wire)|  37 | 26  | close bolt            |High = Activate|
|Green(2wire)| 35 | 19  | open bolt             |High = Activate|
|Green     |  11 | 17   | bolt Open Hall sensor |Low = true|
|Yellow    |  13 | 27   | bolt close Hall sensor|Low = true|
|Blue      |  15 | 22   | door close Hall sensor|Low = true|














#### Hardware notes

##### solenoids and driver
We have two pull only solenoids. Each are ran from a bank of capacitors charged with boost converter. The boost converter can't supply enough power to run the solenoid at 12v and relies on the stored energy in the capacitors which charge to about 36v. The burst doesn't put enough power into the solenoids to damage them. After the initial burst there if the solenoid the boost converter will current limit and out put about 7v. The power transistors used for switching aren't extremely oversized so they cannot take a high duty cycle but can easily handle bursts of a few seconds.

##### USB Card Reader
The [USB Card Reader](https://www.amazon.com/gp/product/B00BYKPHSU/ref=oh_aui_search_detailpage?ie=UTF8&psc=1) registers as a HID device so it look like a keyboard to the RPi. The evdev library might not need to be used at all since the you can just capture the input stream. Googling "capturing HID input with python Raspbery pi" yielded some good results.
