import os
import time
from time import sleep
import RPi.GPIO as GPIO
import string
from evdev import InputDevice
from select import select

GPIO.setmode(GPIO.BCM)
#5 = blue 
#6 = red 
#13 = green 
GPIO.setup(5, GPIO.OUT)
GPIO.setup(6, GPIO.OUT)
GPIO.setup(13, GPIO.OUT)
while True:
	GPIO.output(13,0)
	GPIO.output(5,1)
	time.sleep(.2)
	GPIO.output(5,0)
	GPIO.output(6,1)
	time.sleep(.2)
	GPIO.output(6,0)
	GPIO.output(13,1)	
	time.sleep(.2)

            
