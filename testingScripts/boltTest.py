import os
from time import sleep
import RPi.GPIO as GPIO
import string
from evdev import InputDevice
from select import select
import time

lock = 1
GPIO.setmode(GPIO.BCM)
GPIO.setup( 12, GPIO.OUT)
GPIO.setup( 19, GPIO.OUT)
#26 = red = lock
#19 = green = unllock
while True:
   GPIO.output(12, GPIO.LOW)
   sleep(0.5)   
   GPIO.output(12, GPIO.HIGH)
   sleep(5) 
   GPIO.output(19, GPIO.LOW)
   sleep(0.5)
   GPIO.output(19, GPIO.HIGH)
   sleep(5)
