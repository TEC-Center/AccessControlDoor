import os
import time
from time import sleep
import RPi.GPIO as GPIO
import string
from evdev import InputDevice
from select import select

GPIO.setmode(GPIO.BCM)
#27 = yello = locked
#17 = green = unlocked
#22 = blue  = door closed
GPIO.setup(27, GPIO.IN,pull_up_down=GPIO.PUD_UP)
GPIO.setup(17, GPIO.IN,pull_up_down=GPIO.PUD_UP)
GPIO.setup(22, GPIO.IN,pull_up_down=GPIO.PUD_UP)
while True:
	txt =  "locked "+str(GPIO.input(27))+" "
	txt +=( "unlocked "+str(GPIO.input(17)))+" "
	txt +=( "Door Closed "+str(GPIO.input(22)))
	print(txt)
	time.sleep(.1)

            
