import os
from time import sleep
import RPi.GPIO as GPIO
import string
from evdev import InputDevice
from select import select
import time

reader = InputDevice('/dev/input/by-id/usb-Sycreader_RFID_Technology_Co.__Ltd_SYC_ID_IC_USB_Reader_08FF20140315-event-kbd')
tcnt = 0
lock = 1
GPIO.setmode(GPIO.BCM)
GPIO.setup( 26, GPIO.OUT)
GPIO.setup( 19, GPIO.OUT)
#26 = yello = unlock
#19 = green = lock
while True:
   secCount = 0
   strTime = time.strftime("%Y-%m-%d--%H-%M-%S")
   GPIO.output(26, GPIO.HIGH)
   sleep(0.5)   
   os.system("avconv -f video4linux2 -s 2556x1600 -i /dev/video0 -vframes 1 ~/vidTest/unlocked--"+strTime+".jpg")
   GPIO.output(26, GPIO.LOW)
   sleep(5) 
   GPIO.output(19, GPIO.HIGH)
   sleep(0.5)
   GPIO.output(19, GPIO.LOW)
   os.system("avconv -f video4linux2 -s 2556x1600 -i /dev/video0 -vframes 1 ~/vidTest/locked--"+strTime+".jpg")
   while secCount < 54:
      sleep(1)
      strTime = time.strftime("%Y-%m-%d--%H-%M-%S")
      os.system("avconv -f video4linux2 -s 2556x1600 -i /dev/video0 -vframes 1 ~/vidTest/security/"+strTime+".jpg")
      secCount = secCount+1
