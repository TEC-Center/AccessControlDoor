import os
from time import sleep
import RPi.GPIO as GPIO
import string
from evdev import InputDevice
from select import select
from pymongo import MongoClient
import ssl
import picamera
import json

R = 6
G = 13
B = 5

GPIO.setmode(GPIO.BCM)
GPIO.setup(B, GPIO.OUT)
GPIO.setup(R, GPIO.OUT)
GPIO.setup(G, GPIO.OUT)


def status(r,g,b):
        GPIO.output(R,r)
        GPIO.output(G,g)
        GPIO.output(B,b)

status(1,1,0)

def readSerialNum():
        serial = ""
        keys = "XX1234567890"
        r,w,x = select([reader],[],[])
        for event in reader.read_loop():
                if event.type==1 and event.value==1:
                        if event.code != 28:
                                serial += keys[event.code]
                        else:
                                return serial

def checkList(idList,id):
        for authed in idList:
                if authed['serial'] == id:
                        return authed
        return False

try:
        with open('config.json','r') as confFile:
                conf = json.load(confFile)
except IOError:
        print 'Unable to open config.json'
except: 
        print 'config.json error, check the format of the config file'

if conf:
        try:
                mongo = MongoClient(host=conf['mongo']['host'],port=conf['mongo']['port'],ssl=True, ssl_certreqs=conf['mongo']['ssl_cert_req'])
                db = mongo.get_database(conf['mongo']['db'])
                db.authenticate(name=conf['mongo']['name'],password=conf['mongo']['password'],mechanism=conf['mongo']['mechanism'])
	except:
		print "Unable to connect to mongodb"
else:
        print "No valid Config.json, skipping mongodb connection"

try:
        reader = InputDevice('/dev/input/by-id/usb-Sycreader_RFID_Technology_Co.__Ltd_SYC_ID_IC_USB_Reader_08FF20140315-event-kbd')
except:
        if conf:
                print "Card Reader Error, shit's fucked call: " + conf['support']['phone']
        else:
                print "Card Reader Error, call the administrator!"
        exit




try:
         with open("authList/roles.json","r") as roleFile:
                 authList = json.load(roleFile)
except:
        print 'Error, ensure /authList/roles.json exists and has at least one ID in the "admin" object'
        exit


print 'Please swipe your admin card to add a user'

authed = False
while authed == False:
        serial = readSerialNum()                        
        authed = checkList(authList['admins'],serial)
        if authed:
                break
        print 'You are not on the admin list please scan an admin badge'

status(1,1,1)
print 'Hello '+authed['name']
print 'to add another admin scan your card again'
print 'to add user with access to the space scan that card'
scan2 = readSerialNum()
if scan2 == authed['serial']:
        print 'Now scan the card you would like to add as an admin'
        scan3 = readSerialNum()
        if checkList(authList['admins'],scan3) == False:
                authList['admins'].append({'serial':scan3,'name':""})
                print 'Card with Serial Number: '+scan3+' now has admin privilages'
                with open('authList/roles.json','w+') as tf:
                        json.dump(authList,tf,indent=1)
        else:
                print 'ID is already an admin'
                status(1,0,0)
else:
        if checkList(authList['spaceUsers'],scan2) == False:
                authList['spaceUsers'].append({'serial':scan2,'name':""})
                print 'Card with Serial Number: '+scan2+' now has access to the space'
                with open('authList/roles.json','w+') as tf:
                        json.dump(authList,tf,indent=1)
        else:
                print 'ID is already a spaceUser'
                status(1,0,0)
print 'exiting'
exit
